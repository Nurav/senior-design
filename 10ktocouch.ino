#include <SoftwareSerial.h>
#include <TinyGPSPlus.h>
#include "FS.h"
#include <SPI.h>
#include "Free_Fonts.h" // Include the header file attached to this sketch
#include "SD.h"

#include <TFT_eSPI.h> // Hardware-specific library

#define CALIBRATION_FILE "/TouchCalData3"
#define REPEAT_CAL false

#define TRAINSTATE 1
#define NORMALSTATE 0

#define BUTTON_X  250
#define BUTTON_Y  20
 
byte state = 0;
bool updateScreen = 1;
bool pressed = 1;
bool stoppedTouch = true;
static const int RXPin = 35, TXPin = 34;
static const uint32_t GPSBaud = 9600;
unsigned long thyme = 0;
int currHr = 0, currMin = 0;
String currSpeed = "0.";

int oldHr = 0, oldMin = 0, oldHR = 0;
String oldSpeed = "";
bool prevAM = 0;

TFT_eSPI tft = TFT_eSPI();  
TinyGPSPlus gps;
SoftwareSerial ss(RXPin, TXPin);


void setup() {
  Serial.begin(115200);
  ss.begin(GPSBaud);
  tft.init();
  tft.setRotation(1);
  touch_calibrate();
  delay(200);
  normState(pressed, currHr, currMin, currSpeed);
}

void loop() {
  uint16_t x, y;
  if (tft.getTouch(&x, &y)) {           //Checks if user has pressed the start/stop button. Some debouncing logic as well.
    if (stoppedTouch){
      pressed = buttonPressed(x, y);
      state = state ^ pressed;
    }
    stoppedTouch = false;
  } else {
    stoppedTouch = true;  
  }
  if (millis() - thyme > 100) {               // Reads data from the GPS. Limits Frame rate to 10Hz
    while (ss.available() > 0){
      if (gps.encode(ss.read())) {
        if (gps.time.isValid()) {
          currMin = gps.time.minute();
          currHr = gps.time.hour();
        }
        if (gps.speed.isValid()) {
          currSpeed = String(gps.speed.mph());
          Serial.println(currSpeed);
        }
        updateScreen = true;  
      }
    }
    thyme = millis();
  }
  updateScreen = updateScreen || pressed;                   // Only wants to update the LCD when necessary to conserve processing and to look smoother
  if (updateScreen){
    if (state == NORMALSTATE) {
      normState(pressed, currHr, currMin, currSpeed);
    } else {
      workState(pressed, currHr, currMin, currSpeed);
    }
    updateScreen = false;
  }
  pressed = false;
}

bool buttonPressed(int x, int y){                                // Function used to check if the start/stop button was pressed
  return (sq(x - (BUTTON_X+27)) + sq(y - (BUTTON_Y+7))^2) < 600;
}

unsigned int rgb2Hex(byte red, byte green, byte blue){          // Converts RGB to hex value
  return red << 11 | green << 5 | blue;  
}

void normState(bool forceUpdate, int thymeHr, int thymeMin, String sped) {      // Normal state i.e. not working out
  bool AM =  false;                                                             // Time is offset by 6 hrs for some reason. Fixes this and converts it from a 24 hour clock to a 12 hour
  thymeHr += 18;
  thymeHr = thymeHr % 24;
  AM = thymeHr < 12;
  thymeHr = thymeHr % 12;
  if (!thymeHr)
    thymeHr = 12;
  if (forceUpdate) {                                                            // This code and the code below it is convoluted. It is done to improve smoothness with the limited clock speed. 
    tft.fillScreen(rgb2Hex(200, 0, 0));                                         // Values only udate when they change and rather than the whole screen refreshing, only the previous values are overwritted and new values are written
    tft.setTextColor(TFT_WHITE);
    tft.setFreeFont(FF21);
    tft.setTextSize(1);
    tft.drawString("Start", BUTTON_X, BUTTON_Y);
    tft.drawString("HR: 140", 50, 170);
    
  }
  int textThyme[] = {0, 0};
  bool textAM = 0;
  if (forceUpdate || thymeMin != oldMin) {
    for (byte i = 0; i < 2; i++) {
      textAM = (i ? AM : prevAM);
      textThyme[0] = (i ? thymeMin : oldMin);
      textThyme[1] = (i ? thymeHr : oldHr);
      tft.setTextColor((i ? TFT_WHITE : rgb2Hex(200, 0, 0)));
      tft.setFreeFont(FF24);
      tft.setTextSize(2);
      String currThyme =  String(textThyme[1]) + ":" + (textThyme[0] > 9 ? "" : "0") + String(textThyme[0]);
      if (textThyme[1] < 10)
        currThyme = "0" + currThyme;
      tft.drawString(currThyme, 20, 70);
      tft.setTextSize(1);
      tft.setFreeFont(FF22);
      tft.drawString((textAM ? "AM" : "PM"), 260, 104);
    }
    oldMin = thymeMin;
    oldHr = thymeHr;
    prevAM = AM;
  }
  if (forceUpdate || sped != oldSpeed) {
    tft.setFreeFont(FF21);
    tft.setTextSize(1);
    for (byte i = 0; i < 2; i++) {
      tft.setTextColor((i ? TFT_WHITE : rgb2Hex(200, 0, 0)));
      String speedString = "SPEED: " + (i ? sped : oldSpeed) + "MPH";
      tft.drawString(speedString, 150, 170);
    } 
    oldSpeed = sped;
  }
}

void workState(bool forceUpdate, int thymeHr, int thymeMin, String sped) {              // Workout state. Very similar to the Normal state, with mostly formatting changes
  bool AM =  false;
  thymeHr += 18;
  thymeHr = thymeHr % 24;
  AM = thymeHr < 12;
  thymeHr = thymeHr % 12;
  if (!thymeHr)
    thymeHr = 12;
    
  
  if (forceUpdate) {
    SD.begin(5);                                                                          // Writes random data to the SD card, along with real time
    File file = SD.open("/test.txt", FILE_WRITE);
    file.println("logged Data:");
    for (int i = 0; i <20; i++) {
      file.print(String(thymeHr));
      file.print(":");
      file.print(String(thymeMin));
      file.print(AM ? "AM" : "PM");
      file.print(", ");
      file.print(String(random(2175)));
      file.print("\n");
    }
    file.close();
    tft.fillScreen(TFT_WHITE);
    tft.setTextColor(rgb2Hex(0, 0, 250));
    tft.setFreeFont(FF21);
    tft.setTextSize(1);
    tft.drawString("Stop", BUTTON_X, BUTTON_Y);
    tft.setFreeFont(FF22);
    tft.drawString("HR: 160", 30, 120);
    tft.drawString("DIST: 1.8MI", 30, 180);
    tft.drawString("WORKOUT TIME: 0:14", 30, 90);
      
  }
  int textThyme[] = {0, 0};
  bool textAM = 0;
  if (forceUpdate || thymeMin != oldMin) {
    for (byte i = 0; i < 2; i++) {
      textAM = (i ? AM : prevAM);
      textThyme[0] = (i ? thymeMin : oldMin);
      textThyme[1] = (i ? thymeHr : oldHr);
      tft.setTextColor((i ? rgb2Hex(0, 0, 250) : TFT_WHITE));
      tft.setFreeFont(FF24);
      tft.setTextSize(1);
      String currThyme =  String(textThyme[1]) + ":" + (textThyme[0] > 9 ? "" : "0") + String(textThyme[0]);
      if (textThyme[1] < 10)
        currThyme = "0" + currThyme;
      tft.drawString(currThyme, 30, 30);
      tft.setTextSize(1);
      tft.setFreeFont(FF22);
      tft.drawString((textAM ? "AM" : "PM"), 150, 44);
    }
    oldMin = thymeMin;
    oldHr = thymeHr;
    prevAM = AM;
  }
  if (forceUpdate || sped != oldSpeed) {
    tft.setFreeFont(FF22);
    tft.setTextSize(1);
    for (byte i = 0; i < 2; i++) {
      tft.setTextColor((i ? rgb2Hex(0, 0, 250): TFT_WHITE));
      String speedString = "SPEED: " + (i ? sped : oldSpeed) + "MPH";
      tft.drawString(speedString, 30, 150);
    } 
    oldSpeed = sped;
  }
}


//////////////////// The Below Code is used to calibrate the Touch Screen. Calibration only happens once and resulting data is stored in the MCU's EEPROM. I DID NOT WRITE THE BELOW CODE. It is the only code that is not mine ///////////////////

void touch_calibrate()
{
  uint16_t calData[5];
  uint8_t calDataOK = 0;

  // check file system exists
  if (!SPIFFS.begin()) {
    SPIFFS.format();
    SPIFFS.begin();
  }

  // check if calibration file exists and size is correct
  if (SPIFFS.exists(CALIBRATION_FILE)) {
    if (REPEAT_CAL)
    {
      // Delete if we want to re-calibrate
      SPIFFS.remove(CALIBRATION_FILE);
    }
    else
    {
      File f = SPIFFS.open(CALIBRATION_FILE, "r");
      if (f) {
        if (f.readBytes((char *)calData, 14) == 14)
          calDataOK = 1;
        f.close();
      }
    }
  }
  if (calDataOK && !REPEAT_CAL) {
    // calibration data valid
    tft.setTouch(calData);
  } else {
    // data not valid so recalibrate
    tft.fillScreen(TFT_BLACK);
    tft.setCursor(20, 0);
    tft.setTextFont(2);
    tft.setTextSize(1);
    tft.setTextColor(TFT_WHITE, TFT_BLACK);

    tft.println("Touch corners as indicated");

    tft.setTextFont(1);
    tft.println();

    if (REPEAT_CAL) {
      tft.setTextColor(TFT_RED, TFT_BLACK);
      tft.println("Set REPEAT_CAL to false to stop this running again!");
    }

    tft.calibrateTouch(calData, TFT_MAGENTA, TFT_BLACK, 15);

    tft.setTextColor(TFT_GREEN, TFT_BLACK);
    tft.println("Calibration complete!");

    // store data
    File f = SPIFFS.open(CALIBRATION_FILE, "w");
    if (f) {
      f.write((const unsigned char *)calData, 14);
      f.close();
    }
  }
}
